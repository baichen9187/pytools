class LoginException(Exception):

    def __init__(self, message):  # real signature unknown
        super().__init__(self)
        self.message = message

    def __str__(self):
        return self.message
