from threading import Thread


class ThreadUtil(Thread):

    def __init__(self, func, args=()):
        super(ThreadUtil, self).__init__()
        self._result = None
        self.func = func
        self.args = args
        self._exception = None

    def run(self):
        try:
            self._result = self.func(*self.args)
        except Exception as e:
            self._exception = e
            raise e

    def get_result(self, timeout=None):
        self.join(timeout)
        return self._result

    def get_exception(self) -> Exception | None:
        return self._exception
