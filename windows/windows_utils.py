import random
import sys

if sys.platform.startswith('linux'):
    # linux 代码
    pass
elif sys.platform.startswith('freebsd'):
    # freebsd 代码
    pass
elif sys.platform.startswith("win32"):
    import win32gui
    import win32con
    import win32api
import time


def findTitle(window_title):
    '''
    查找指定标题窗口句柄
    @param window_title: 标题名
    @return: 窗口句柄
    '''
    hWndList = []
    # 函数功能：该函数枚举所有屏幕上的顶层窗口，办法是先将句柄传给每一个窗口，然后再传送给应用程序定义的回调函数。
    win32gui.EnumWindows(lambda hWnd, param: param.append(hWnd), hWndList)
    for hwnd in hWndList:
        # 函数功能：该函数获得指定窗口所属的类的类名。
        # clsname = win32gui.GetClassName(hwnd)
        # 函数功能：该函数将指定窗口的标题条文本（如果存在）拷贝到一个缓存区内
        title = win32gui.GetWindowText(hwnd)
        if (window_title in title):
            return title, hwnd
    return ()


def windows_max(handle):
    win32gui.SetForegroundWindow(handle)
    win32gui.ShowWindow(handle, 3)  # 窗口最大化


def move(handle: int, point: tuple[int], move_point: tuple[int]):
    """
        后台移动鼠标
    """
    try:
        # 激活窗口刀前台
        win32gui.SetForegroundWindow(handle)
        win32gui.ShowWindow(handle, 3)  # 窗口最大化
        left, top, right, bottom = win32gui.GetWindowRect(handle)
        width = right - left
        height = bottom - top
        # # 计算指定检查点的坐标
        x = left + int(point[0] * width)
        y = top + int(point[1] * height)

        x1 = left + int(point[0] * width) + random.randint(1, 20)
        y1 = top + int(point[1] * height)
        # x = point[0]
        # y = point[1]
        # 移动鼠标指针
        win32api.SetCursorPos(point)
        win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN, x, y, 0, 0)  # 鼠标左键按下
        # time.sleep(0.5)
        # win32api.SetCursorPos(move_point)
        for i in range(x, x1):
            win32api.mouse_event(win32con.MOUSE_MOVED, i, y1, 0, 0)  # 鼠标左键按下
        win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP, x1, y1, 0, 0)  # 鼠标左键抬起
    except Exception as e:
        pass


def clear_cookie(handle: int, point, point2, point3, point4):
    win32gui.SetForegroundWindow(handle)
    win32gui.ShowWindow(handle, 3)  # 窗口最大化
    # left, top, right, bottom = win32gui.GetWindowRect(handle)
    # width = right - left
    # height = bottom - top
    # # # 计算指定检查点的坐标
    # x = left + int(point[0] * width)
    # y = top + int(point[1] * height)
    x = point[0]
    y = point[1]
    win32api.SetCursorPos(point)
    win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN, x, y, 0, 0)  # 鼠标左键按下
    win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP, x, y, 0, 0)  # 鼠标左键抬起
    x = point2[0]
    y = point2[1]
    win32api.SetCursorPos(point2)
    win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN, x, y, 0, 0)  # 鼠标左键按下
    win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP, x, y, 0, 0)  # 鼠标左键抬起
    x = point3[0]
    y = point3[1]
    win32api.SetCursorPos(point3)
    time.sleep(0.1)
    for i in range(15):
        win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN, x, y, 0, 0)  # 鼠标左键按下
        win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP, x, y, 0, 0)  # 鼠标左键抬起
    x = point4[0]
    y = point4[1]
    win32api.SetCursorPos(point4)
    win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN, x, y, 0, 0)  # 鼠标左键按下
    win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP, x, y, 0, 0)  # 鼠标左键抬起
    time.sleep(0.1)
    win32api.keybd_event(0x0D, 0, 0, 0)
    win32api.keybd_event(0x0D, 0, win32con.KEYEVENTF_KEYUP, 0)


def find_hwnd(hwndChildList: list, window_title):
    for hwnd in hwndChildList:
        # 函数功能：该函数获得指定窗口所属的类的类名。
        # clsname = win32gui.GetClassName(hwnd)
        # 函数功能：该函数将指定窗口的标题条文本（如果存在）拷贝到一个缓存区内
        title = win32gui.GetWindowText(hwnd)
        if (window_title == title):
            return title, hwnd


def get_child_windows(parent):
    '''
    获得parent的所有子窗口句柄
     返回子窗口句柄列表
     '''
    if not parent:
        return
    hwndChildList = []
    win32gui.EnumChildWindows(parent, lambda hwnd, param: param.append(hwnd), hwndChildList)
    return hwndChildList


if __name__ == "__main__":
    window_title = u'千牛工作台'
    hwnd = findTitle(window_title)
    # move(hwnd[1], (4077, 583), (4823, 623))
    # clear_cookie(hwnd[1], (3032, 51), (3128, 174), (3736, 548), (4006, 554))
    # find_hwnd(get_child_windows(hwnd[1]), "")
    # clear_cookie(hwnd[1], (144, 53), (192, 173), (624, 558), (900, 549))  # 生产
    # move(hwnd[1], (844, 531), (1299, 539))
    # {
    #     "start": [[844, 531], [844, 531], [844, 531]],
    #     "end": [[1299, 539], [1289, 579], [1299, 660]]
    # }
