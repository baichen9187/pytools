import redis
import json

from functools import wraps
from common.logger import logger
from common.globalconfig import global_config


class RedisUtil:
    PROXY_IP = "proxy:ips"
    cursor: redis.Redis = None

    @staticmethod
    def is_check():
        if RedisUtil.cursor is None:
            RedisUtil.cursor = redis.Redis(host=global_config.REDIS_ACTIVE.get('host'),
                                           port=global_config.REDIS_ACTIVE.get('port'),
                                           password=global_config.REDIS_ACTIVE.get('password'),
                                           db=global_config.REDIS_ACTIVE.get('db'))
            logger.debug("Redis init")

    @staticmethod
    def members(key):
        RedisUtil.is_check()
        logger.debug(f"members: {key}")
        return RedisUtil.cursor.smembers(key)

    @staticmethod
    def set(key, value, ex=None, **kwargs):
        RedisUtil.is_check()
        return RedisUtil.cursor.set(key, value, ex, **kwargs)

    @staticmethod
    def get(key):
        RedisUtil.is_check()
        return RedisUtil.cursor.get(key)

    @staticmethod
    def close():
        logger.debug("Redis close")
        if RedisUtil.cursor:
            RedisUtil.cursor.close()

    @staticmethod
    def clear(key):
        RedisUtil.is_check()
        RedisUtil.cursor.delete(key)


def cache(
        expire: int = None,
        namespace="",
):
    """
    cache all function
    :param namespace:
    :param expire:
    :return:
    """

    def wrapper(func):
        @wraps(func)
        def inner(*args, **kwargs):
            prefix = global_config.REDIS_ACTIVE.get('prefix')
            _args = ":".join([str(i) for i in list(args) if i and not isinstance(i, dict)]) if args else ""
            _kwargs = ":".join([str(v) for k, v in kwargs.items()]) if kwargs else ""
            cache_key = f"{prefix}:{namespace}:{func.__name__}"
            if _args:
                cache_key += f":{_args}"
            if _kwargs:
                cache_key += f":{_kwargs}"
            ret = RedisUtil.get(cache_key)
            if ret is not None:
                return json.loads(ret)
            # 如果没有缓存
            try:
                ret = func(*args, **kwargs)
            except Exception as e:
                RedisUtil.clear(cache_key)
                raise e
            RedisUtil.set(cache_key, json.dumps(ret), ex=expire)
            return ret

        return inner

    return wrapper


def cache_async(
        expire: int = None,
        namespace="",
):
    """
    cache all function
    :param namespace:
    :param expire:
    :return:
    """

    def wrapper(func):
        @wraps(func)
        async def inner(*args, **kwargs):
            prefix = global_config.REDIS_ACTIVE.get('prefix')
            _args = ":".join([str(i) for i in list(args) if i and not isinstance(i, dict)]) if args else ""
            _kwargs = ":".join([v for k, v in kwargs.items()]) if kwargs else ""
            cache_key = f"{prefix}:{namespace}:{func.__name__}"
            if _args:
                cache_key += f":{_args}"
            if _kwargs:
                cache_key += f":{_kwargs}"
            ret = RedisUtil.get(cache_key)
            if ret is not None:
                return json.loads(ret)
            # 如果没有缓存
            try:
                ret = await func(*args, **kwargs)
            except Exception as e:
                RedisUtil.clear(cache_key)
                raise e
            RedisUtil.set(cache_key, json.dumps(ret), ex=expire)
            return ret

        return inner

    return wrapper


Cache = cache
CacheAsync = cache_async


class APlan:
    def __init__(self, name, _list, ex: int):
        self._func = None
        self._list = _list
        self.index = 0
        self.name = name
        self._ex = ex

    def init(self):
        self._func = None
        self.index = 0

    def __iter__(self):
        return self

    def __next__(self):
        @cache(self._ex, namespace=f"{self.name}:{self.index}")
        def _run():
            return next(self._list)

        _data = _run()
        self.index += 1
        return _data
