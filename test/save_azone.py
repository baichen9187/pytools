from common.excel_utils import read_excel
from common.db import DB
from common.utils import parser_data

tmp_dim_adZone_columns_map = ["推广位ID", "UP主账号名称", "联盟账号ID"]
tmp_dim_adZone_columns = ["[推广位ID]", "[UP主账号名称]", "[联盟账号ID]"]


def main():
    mydb = DB(database='lkm')
    xlsx = read_excel(r"C:\Users\bai\Downloads\更正.xlsx", sheet_name=0)
    columns = xlsx.columns
    data = [dict(zip(columns, i)) for i in xlsx.values]
    data_list = []
    for i in data:
        if i.get("联盟账号ID"):
            try:
                i["推广位ID"] = int(i["推广位ID"])
                i["联盟账号ID"] = int(i["联盟账号ID"])
                data_list.append(i)
            except:
                pass
        # for k, v in i.items():
        #     i[k] = str(v)
    #     d = {"UP主账号名称": i.get("账号昵称")}
    #     if v := i.get("京东pid"):
    #         ids = v.split("_")
    #         if len(ids) > 2:
    #             d["推广位ID"] = ids[2]
    #             d["联盟账号ID"] = ids[0]
    #     if i.get("淘宝Pid") and type(i.get("淘宝Pid")) == str:
    #         ids = i.get("淘宝Pid").split("_")
    #         if len(ids) > 3:
    #             d["推广位ID"] = ids[3]
    #             d["联盟账号ID"] = ids[1]
    #     data_list.append(d)
    mydb.insertOrUpdateById('tmp_dim_adZone', tmp_dim_adZone_columns_map, data_list,
                    "推广位ID")
    mydb.commit()
    mydb.close()


if __name__ == "__main__":
    main()