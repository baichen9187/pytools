import re
import asyncio
from common.mysql_db import MysqlPool
from common.async_utils import AsyncUtils

pool: MysqlPool = MysqlPool(url="mysql://root:wt123456@47.98.98.230:33006/data_warehouse?charset=utf8")


def main():
    get_order_list = pool.fetch_all("select order_num,spread_name,pay_at from sv_playlet_order where editing is null",
                                    columns=["order_num", "spread_name", "pay_at"])
    data = []
    for order in get_order_list:
        if spread_name := order.get("spread_name"):
            editings = re.split("-|—", spread_name)
            order["editing"] = editings[-1] if editings is not None and len(editings) > 0 else None
            order["book_name"] = editings[0] if editings is not None and len(editings) > 0 else None
        order["date"] = str(order["pay_at"].date())
        pool.update("update sv_playlet_order set book_name=%s,editing=%s,date=%s where order_num = %s",
                    (order["book_name"], order["editing"], order["date"], order["order_num"]))
        pool.commit()



if __name__ == "__main__":
    main()
    pool.close()
