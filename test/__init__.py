from common.logger import logger, logger_file
from common.globalconfig import global_config

logger.info("__init__")
logger.add(
    logger_file(name="test/login"),
    encoding=global_config.GLOBAL_ENCODING,
    level=global_config.LOGGER_LEVEL,
    rotation=global_config.LOGGER_ROTATION,
    retention=global_config.LOGGER_RETENTION,
    enqueue=True,
    format=global_config.LOGGER_FORMAT
)