import gc
import time
from common.ThreadSafeDict import ThreadSafeDict
from requests.sessions import Session

data = ThreadSafeDict()

class T:
    def __init__(self):
        self.data = 1


def set():
    for i in range(100000):
        data.set(i, T())


def get():
    _data = {}
    for k, v in data.items():
        _data[k] = v
        # time.sleep(0.1)
    return _data


def del_():
    c = get()
    for k, v in c.items():
        data.delete(k)
        pass
    del c
    gc.collect()


if __name__ == "__main__":
    set()
    d = get()
    print(len(d))
    time.sleep(5)
    del_()
    time.sleep(10)
    print(len(d))
    print(sum([v.data for k,v in d.items()]))
    print(len(get()))