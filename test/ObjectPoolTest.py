import unittest

from common.ObjectPool import ObjectPool
from db.db import DB


class ObjectPoolTest(unittest.TestCase):


    def test_object_pool(self):
        pool = ObjectPool(lambda : DB(), 10)
        tmp = list()
        with pool:
            for _ in range(5):
                obj = pool.acquire()
                tmp.append(obj)
                print(f"db: {id(obj)}")
            for i in tmp:
                pool.release(i)
            print(pool.size())
            for _ in range(20):
                obj = pool.acquire()
                print(f"db: {id(obj)}")
