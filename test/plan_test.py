from common.LoginUtil import LoginUtil
from common.SpiderPlan import SpiderPlan
from common.db import DB
from common.logger import logger
from common.utils import retry, get_date_range
from spider.server.Blibli import Bilibili


def test_login():
    print(LoginUtil.tb(5517477571))
    print(LoginUtil.jm("博朗得", 19))
    print(LoginUtil.blibli_shop("15968819780"))

def bili_test():
    bili = Bilibili()
    name = bili.update_up_name(3493134891747396, proxy=True)
    print(name)


def spiderPlan_test():
    db = DB(database='qifei')
    spiderPlan = SpiderPlan(db, "test", list(range(20)))
    try:
        for item in spiderPlan:
            print(item)
    except Exception as e:
        raise
    finally:
        spiderPlan.write()
        db.close()


def test_time():
    logger.info(get_date_range(1, -31)[0])
    logger.info(get_date_range(1, 0)[1])


if __name__ == "__main__":
    logger.info(f"{__name__}测试成功")
    # Test.error()
    # bili_test()
    spiderPlan_test()
    # print(with_test())
    # print(get_cookies(40))
