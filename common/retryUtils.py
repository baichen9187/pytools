import asyncio
import functools
import time


def retry(retry_count):
    def decorator_retry(func):
        @functools.wraps(func)
        def wrapper_retry(*args, **kwargs):
            current_retry = 0
            while current_retry < retry_count:
                try:
                    result = func(*args, **kwargs)
                    return result
                except Exception as e:
                    current_retry += 1
                    print(f"Function {func.__name__} failed. Retry {current_retry}/{retry_count}. Error: {str(e)}")
                    time.sleep(1)
        return wrapper_retry
    return decorator_retry


def async_retry(retry_count):
    def decorator_retry(func):
        @functools.wraps(func)
        async def wrapper_retry(*args, **kwargs):
            current_retry = 0
            while current_retry < retry_count:
                try:
                    result = await asyncio.create_task(func(*args, **kwargs))
                    return result
                except Exception as e:
                    current_retry += 1
                    print(f"Function {func.__name__} failed. Retry {current_retry}/{retry_count}. Error: {str(e)}")
                    await asyncio.sleep(1)
        return wrapper_retry
    return decorator_retry