import pandas as pd


def read_excel(path, **kwargs):
    return pd.read_excel(io=path,  **kwargs)


def js_from_file(file_name):
    """
    读取js文件
    :return:
    """
    with open(file_name, 'r', encoding='UTF-8') as file:
        result = file.read()
    return result