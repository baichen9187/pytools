# -*- coding: UTF-8 -*-

from pydantic import BaseSettings
import os
import sys


class GlobalConfig(BaseSettings):
    """
       全局通用配置
    """
    argv = {}
    # redis
    REDIS_ACTIVE = {"prefix": "lkm_python"}
    for i in sys.argv:
        values = i.split("=")
        if len(values) > 1:
            argv.update({values[0]: values[1]})

    # 激活配置
    active: str = argv.get("active") if argv.get("active") else "prod"

    # 全局
    GLOBAL_ENCODING: str = 'utf-8'  # 全局编码

    # 日志模块
    LOGGER_DIR: str = os.path.abspath(os.path.join(os.path.dirname(__file__), "../logs"))  # 日志文件夹名
    LOGGER_NAME: str = '{time:YYYY-MM-DD}.log'  # 日志文件名 (时间格式)
    LOGGER_LEVEL: str = 'DEBUG'  # 日志等级: ['DEBUG' | 'INFO']
    LOGGER_ROTATION: str = "1 days"  # 日志分片: 按 时间段/文件大小 切分日志. 例如 ["500 MB" | "12:00" | "1 week"]
    LOGGER_RETENTION: str = "30 days"  # 日志保留的时间: 超出将删除最早的日志. 例如 ["1 days"]
    LOGGER_FORMAT: str = "{time:YYYY-MM-DD HH:mm:ss} | {level} | {module} | {process.name} | {thread.name} | {name}" \
                         ":{function}:{line}: {message}"
    LOGGER_MODE = 'a+'

    CHROME_ENDPOINT_URL: str = "http://localhost:8888"

    EMPTY = {}
    EMPTY_LIST = []

    active_db: str = "mysql"


global_config = GlobalConfig()
