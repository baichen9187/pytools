import time

from common.logger import logger


def calc_time(func):
    def wrapper(*args, **kwargs):
        current_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
        start_time = time.time()
        result = func(*args, **kwargs)
        end_time = time.time()
        logger.debug(
            f"[{current_time}] Function {func.__name__} took {(end_time - start_time):.4f} seconds to execute.")
        return result

    return wrapper


def calc_async_time(func):
    async def wrapper(*args, **kwargs):
        start_time = time.monotonic()
        result = await func(*args, **kwargs)
        end_time = time.monotonic()
        logger.info(f"Coroutine {func.__name__} took {(end_time - start_time):.6f} seconds to complete.")
        return result

    return wrapper


def calc_self_time(func):
    def wrapper(self, *args, **kwargs):
        start_time = time.time()
        result = func(self, *args, **kwargs)
        end_time = time.time()
        logger.debug(f"Method {func.__name__} took {(end_time - start_time):.4f} seconds to execute.")
        return result

    return wrapper


def calc_self_async_time(func):
    async def wrapper(self, *args, **kwargs):
        start_time = time.monotonic()
        result = await func(self, *args, **kwargs)
        end_time = time.monotonic()
        logger.debug(f"Coroutine {func.__name__} took {(end_time - start_time):.6f} seconds to complete.")
        return result

    return wrapper
