# pytools

#### 介绍
本文是python日常使用中的一些工具类，收集整理了一下，包括了缓存处理，dbUtils,密文加密，字符串处理，asyncio工具类，windows工具，图片集合
其中使用了大量装饰器，进行缓存处理等等

#### 软件架构
软件架构说明
- async 异步工具（asyncio,线程）
- cache 缓存工具
- common 通用（字符串，时间，日志，重试，对象池，计时器，超时器）
- db 数据库工具（mysql,和sqlserver）
- crypto 加密处理 RSA
- windows 系统工具，主要用户控制鼠标，滑动
- test 测试集合
#### 快速使用

```python
from cache.redis_util import cache, cache_async
from common.retryUtils import retry, async_retry
from common.timeUtil import calc_time, calc_async_time

@calc_time
@retry(3)
@cache(60 * 10, namespace="test")
def _get_account(name):
    return {"test":name}

@calc_async_time
@async_retry(3)
@cache_async(60 * 10, namespace="test")
async def _get_account(name):
    return {"test":name}
```


#### 安装教程
暂时没有上传, 直接下载代码即可

